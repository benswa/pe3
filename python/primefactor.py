"""
Project Euler Problem 3

The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?

Use while loop and keep track of a number that should be between 2 and number_of_interest
and see whether current number is a multiple of number_of_interest by using division. If it is,
divide number_of_interest by that number and continue looping using that same number. When the number you
are on can no longer divide into number_of_interest then this is when you increase the the current number by 1.

Any value that cannot be divided into a value evenly is not a factor.
"""


__author__ = 'Ben'


def is_prime(prime_list, number):
    """
    This function is for checking to see if the number passed in is divisible all the prime numbers
    on the list. If it is not divisible by any of the prime numbers. This function will give a false
    positive if the list of prime numbers are not big enough and your number that you passed in as input
    is sufficiently large that it cannot be divided by any of the primes on the list.

    Range: 1 to infinity.

    This function will not modify the list.
    """

    # Prevents this function from being used if input parameter is below zero.
    assert (number > 0), "is_prime: Input value, number is zero or less."

    # Prevents this function from being used if the input list is length zero or less.
    assert (len(prime_list) > 0), "is_prime: Input list, length of prime_list is zero or less."

    # Checks edge cause where the input number is 1.
    if number == 1.0:
        return False

    # Loops through the list that was passed in and is assumed all the values
    # on that list are prime numbers. Checks to see if the number passed in
    # is divisible by the list of prime numbers.
    for index in range(len(prime_list)):
        if (number % prime_list[index]) == 0.0:
            return False

    # Only reaches here if the number is not divisible by the list of prime numbers
    # and is now considered a prime.
    return True


def find_next_prime(prime_list):
    """
    This function will find the next prime number that comes after the last highest prime number
    on the list.

    This function will modify the list when the next prime number is found by adding the new prime
    number to the back of the list.
    """

    # Prevents this function from being used if the input list is length zero or less.
    assert (len(prime_list) > 0), "is_prime:  length of prime_list is zero or less."

    prime_offset = 1.0
    while True:
        # Taking last value on the list and offsetting it higher to check for new primes
        potential_prime_number = prime_list[-1] + prime_offset
        if is_prime(prime_list, potential_prime_number) is True:
            # Once the new prime number is found, add the new number to the list and return.
            prime_list.append(potential_prime_number)
            return
        else:
            prime_offset += 1.0


if __name__ == '__main__':
    # Separate list to hold the list of prime numbers up to value number_of_interest
    prime_number_list = [2.0, 3.0, 5.0, 7.0]

    number_of_interest = 600851475143.0

    # What is declared below is used for factoring the number_of_interest and keeping track
    # of all the factors that make up the number_of_interest
    prime_factors = list()
    factored_value = number_of_interest
    division_factor = 2.0   # Minimum prime number

    while division_factor <= number_of_interest:
        if factored_value == 1.0:
            # If the factored_value has been completely factored, then break out of the loop.
            break

        if (factored_value / division_factor).is_integer() is False:
            # Uses division to test and see if the factored_value can be divided
            # evenly without remainder by the division_factor.
            division_factor += 1.0
        else:
            # If the number can be factored by the current modulo value, then divide the
            # factored_value by modulo_value and store the modulo value in prime_factors list.
            factored_value /= division_factor
            prime_factors.append(division_factor)

    # Finding the largest prime factor.
    print('Largest prime factor is: ' + str(prime_factors[-1]))